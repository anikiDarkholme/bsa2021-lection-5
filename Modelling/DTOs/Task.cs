﻿using System;
using System.Text.Json.Serialization;

namespace Projects.Modelling.DTOs
{
	public class Task : ModelBase
	{
		[JsonPropertyName("projectId")]
		public int? ProjectId { get; set; }

		[JsonPropertyName("performerId")]
		public int? PerformerId { get; set; }

		[JsonPropertyName("description")]
		public string Description { get; set; }

		[JsonPropertyName("state")]
		public State State { get; set; }

		[JsonPropertyName("createdAt")]
		public DateTime CreatedAt { get; set; }

		[JsonPropertyName("finishedAt")]
		public DateTime? FinishedAt { get; set; }
	}

	public enum State
    {
		Unbegun,
		InProgress,
		Finished,
		Canceled,
    }
}
