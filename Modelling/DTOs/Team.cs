﻿using System;
using System.Text.Json.Serialization;

namespace Projects.Modelling.DTOs
{
	public class Team : ModelBase
	{

		[JsonPropertyName("createdAt")]
		public DateTime CreatedAt { get; set; }
	}
}
