﻿using Projects.Modelling.DTOs;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projects.Modelling.Entities
{
	public class TaskEntity : ModelBase
	{
        public TaskEntity()
        {

        }
        public TaskEntity(Task taskModel, UserEntity performerEntity)
        {
			Id = taskModel.Id;
			ProjectEntityId = taskModel.ProjectId;
			UserEntity = performerEntity;
			Name = taskModel.Name;
			Description = taskModel.Description;
			State = taskModel.State;
			CreatedAt = taskModel.CreatedAt;
			FinishedAt = taskModel.FinishedAt;
        }

        public ProjectEntity ProjectEntity { get; set; }

        public int? ProjectEntityId { get; set; }

		public UserEntity UserEntity { get; set; }

        public int? UserEntityId { get; set; }

        [Column(TypeName = "nvarchar(100)")]
		public new string Name { get; set; }

		[Column(TypeName = "nvarchar(300)")]
		public string Description { get; set; }

		public State State { get; set; }

		[Column(TypeName = "datetime")]
		public DateTime CreatedAt { get; set; }

		[Column(TypeName = "datetime")]
		public DateTime? FinishedAt { get; set; }

		public override string ToString()
        {
			return $"Task ---------\n" +
				$"Id : {Id}|\n" +
				$"Name : {Name}|\n " +
				$"Description : {Description}|\n " +
				$"State : {State}|/n" +
				$"CreatedAt : {CreatedAt}|\n " +
				$"Finished At : {FinishedAt}|" +
				$"Performer Id : {UserEntityId}"+
				$"Project Entity Id : {ProjectEntityId}" +
				$"\n";
		}
	}
}

