﻿using Microsoft.AspNetCore.Mvc.Testing;
using Projects.API.ExtensionsConfiguration;
using Projects.Modelling;
using Projects.Modelling.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Projects.API.IntegrationTesting
{
    [Collection("sync")]
    public class DeleteBehavioursIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly ITestOutputHelper loghelper;
        private HttpClient _client;

        public DeleteBehavioursIntegrationTests(CustomWebApplicationFactory<Startup> factory, ITestOutputHelper loghelper)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });

            SeedData.Init();

            this.loghelper = loghelper;
        }

        [Theory]
        [InlineData("UUser", "users", typeof(User))]
        [InlineData("UTeam", "teams", typeof(Team))]
        [InlineData("UTask", "tasks", typeof(Modelling.DTOs.Task))]
        [InlineData("UProject", "projects", typeof(Project))]
        public async System.Threading.Tasks.Task Delete_WhenEntityExists_ReturnsDeleted(string propertyName, string endpoint, Type modelType)
        {
            //Arrange

            var model = typeof(SeedData)
                 .GetProperty(propertyName, System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public)
                 .GetValue(null);

            string jsonValue = JsonSerializer.Serialize(value: model, inputType: modelType);

            //Act

            var postResponse = await
            _client.PostAsync($"{_client.BaseAddress}api/{endpoint}", new StringContent(jsonValue, encoding: Encoding.UTF8, "application/json"));

            var returnedValue = JsonSerializer.Deserialize(
                await postResponse.Content.ReadAsStringAsync(), returnType: modelType) as ModelBase;

            var delResponse = await
            _client.DeleteAsync($"{_client.BaseAddress}api/{endpoint}/{returnedValue.Id}");

            //Assert

            Assert.Equal(delResponse.StatusCode, System.Net.HttpStatusCode.NoContent);

            Assert.True(returnedValue.Id > 0);
        }
    }
}
