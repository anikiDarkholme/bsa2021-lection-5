﻿using Microsoft.AspNetCore.Mvc.Testing;
using Projects.API.ExtensionsConfiguration;
using Projects.Modelling;
using Projects.Modelling.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Projects.API.IntegrationTesting
{
    [Collection("sync")]
    public class PutBehavioursIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly ITestOutputHelper loghelper;
        private HttpClient _client;

        public PutBehavioursIntegrationTests(CustomWebApplicationFactory<Startup> factory, ITestOutputHelper loghelper)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });

            SeedData.Init();

            this.loghelper = loghelper;
        }

        [Theory]
        [InlineData("UUser", "users", typeof(User))]
        [InlineData("UTeam", "teams", typeof(Team))]
        [InlineData("UTask", "tasks", typeof(Modelling.DTOs.Task))]
        [InlineData("UProject", "projects", typeof(Project))]
        public async System.Threading.Tasks.Task Put_WhenEntityDoesntExist_Returns201WithSameNameAndIdGreaterThan0(string propertyName, string endpoint, Type modelType)
        {
            //Arrange

            var model = typeof(SeedData)
                 .GetProperty(propertyName, System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public)
                 .GetValue(null);

            string jsonValue = JsonSerializer.Serialize(value: model, inputType: modelType);

            //Act

            var getResp = await 
                _client.GetAsync($"{_client.BaseAddress}api/{endpoint}");
            loghelper.WriteLine($"GET All response body: {await getResp.Content.ReadAsStringAsync()}");



            var delResp = await 
                _client.DeleteAsync($"{_client.BaseAddress}api/{endpoint}/1");
            loghelper.WriteLine($"Delete on Id 1 resopnse : {delResp}");

 
            var response = await
            _client.PutAsync($"{_client.BaseAddress}api/{endpoint}/1", new StringContent(jsonValue, encoding: Encoding.UTF8, "application/json"));

            loghelper.WriteLine("Got a response with code: " + response.StatusCode);

            var returnedValue = JsonSerializer.Deserialize(
                await response.Content.ReadAsStringAsync(), returnType: modelType) as ModelBase;

            //Assert

            Assert.Equal(response.StatusCode, System.Net.HttpStatusCode.Created);

            Assert.Equal(model.GetType().GetProperty("Name").GetValue(model), returnedValue.Name);

            Assert.True(returnedValue.Id > 0);
        }
    }
}
