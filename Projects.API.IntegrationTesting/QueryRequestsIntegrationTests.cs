﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Projects.API.ExtensionsConfiguration;
using Projects.Modelling;
using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Projects.API.IntegrationTesting
{
    [Collection("sync")]
    public class QueryRequestsIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly ITestOutputHelper loghelper;
        private HttpClient _client;

        public QueryRequestsIntegrationTests(CustomWebApplicationFactory<Startup> factory, ITestOutputHelper loghelper)
        {
            SeedData.Init();

            _client = factory
                .WithWebHostBuilder(config =>
            {
                config.ConfigureServices(services =>
                {
                    var sp = services.BuildServiceProvider();

                    using (var scope = sp.CreateScope())
                    {
                        var scopedServices = scope.ServiceProvider;
                        var db = scopedServices.GetRequiredService<ProjectsContext>();

                        db.Database.EnsureDeleted();
                        db.Database.EnsureCreated();

                        db.Users.AddRange(SeedData.Users);
                        db.Projects.AddRange(SeedData.Projects);
                        db.Teams.AddRange(SeedData.Teams);
                        db.Tasks.AddRange(SeedData.Tasks);

                        db.SaveChanges();
                    };
                });
            })
                .CreateClient(new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });

            this.loghelper = loghelper;
        }

        [Fact] 
        public async System.Threading.Tasks.Task UnhandledTasks_WhenCalledOnUser2_ReturnsTwoTasksWithIds34()
        {
            //Arrange

            //Act
            var response = await
                _client.GetAsync($"api/misc/unhandledTasks/2");

            var unhandledtasksString = await
                response.Content.ReadAsStringAsync();

            var unhandledTasks = JsonSerializer.Deserialize<IEnumerable<Modelling.DTOs.Task>>(unhandledtasksString,
                new JsonSerializerOptions() { ReferenceHandler = ReferenceHandler.Preserve });

            //Assert

            Assert.Collection(
                unhandledTasks,
                n =>
                {
                    Assert.Equal(n.Id, 3);
                },
                n1 =>
                {
                    Assert.Equal(n1.Id, 4);
                });
        }

        [Fact]
        public async System.Threading.Tasks.Task UnhandledTasks_WhenCalledOnUser1_ReturnsOneTaskWithId1()
        {
            //Arrange

            //Act

            var response = await
                _client.GetAsync($"api/misc/unhandledTasks/1");

            var unhandledtasksString = await
                response.Content.ReadAsStringAsync();

            var unhandledTasks = JsonSerializer.Deserialize<IEnumerable<TaskEntity>>(unhandledtasksString,
                new JsonSerializerOptions() { ReferenceHandler = ReferenceHandler.Preserve });

            //Assert

            Assert.Collection(
                unhandledTasks,
                n =>
                {
                    Assert.Equal(n.Id, 1);
                });
        }
    }
}