﻿using AutoMapper;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Projects.API.ExtensionsConfiguration;
using Projects.API.Interfaces;
using Projects.DataAccess.Interfaces;
using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Projects.API.Services.Testing
{
    public class DTOHandlerServiceTests
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly IDTOHandlerService handler;
        public DTOHandlerServiceTests()
        {
            unitOfWork = A.Fake<IUnitOfWork>();

            var config = new MapperConfiguration(config => config.AddProfile<AutoMapperConfig>());

            handler = new DTOHandlerService(unitOfWork, config.CreateMapper());
        }

        [Theory]
        [InlineData(26)]
        [InlineData(-26)]
        [InlineData(0)]
        public async System.Threading.Tasks.Task TryAddUserAsync_WhenAddingWithId_ThenReturnedMustHaveSameId(int id)
        {
            //Arrange

            SeedData.Users.Clear();

            A.CallTo(() => unitOfWork.Users.AddAsync(A<UserEntity>.Ignored))
                .Invokes((UserEntity user) => SeedData.Users.Add(user));

            A.CallTo(() => unitOfWork.Users.GetByIdAsync(A<int>.Ignored))
                .ReturnsLazily(fake =>
                System.Threading.Tasks.Task.Run(() => SeedData.Users.SingleOrDefault(n => n.Id == id)));

            //Act

            var addedUser =
            await handler.TryAddUserAsync(new User() { Id = id });

            //Assert

            Assert.Equal(id, SeedData.Users.Single().Id);
        }

        [Theory]
        [InlineData(10)]
        [InlineData(0)]
        public async System.Threading.Tasks.Task TryAddUserAsync_WhenTryingToAddExisting_ThrowsArgumentException(int id)
        {
            //Arrange

            SeedData.Users.Clear();

            A.CallTo(() => unitOfWork.Users.AddAsync(A<UserEntity>.Ignored))
                .Invokes((UserEntity user) => SeedData.Users.Add(user));

            A.CallTo(() => unitOfWork.Users.GetByIdAsync(A<int>.Ignored))
                .ReturnsLazily(fake =>
                System.Threading.Tasks.Task.Run(() => SeedData.Users.SingleOrDefault(n => n.Id == id)));


            //Act

            var addedUser =
            await handler.TryAddUserAsync(new User() { Id = id });

            //Assert

            await Assert.ThrowsAsync<ArgumentException>
                 (async () => await handler.TryAddUserAsync(new User() { Id = id }));
        }


        [Theory]
        [InlineData(1, State.InProgress, State.Finished)]
        [InlineData(int.MinValue, State.Unbegun, State.Canceled)]
        public async System.Threading.Tasks.Task ChangeTaskState_WhenTryingToChangeTask_StateStaysFinal(int id, State initial, State final)
        {
            //Arrange

            SeedData.Tasks.Clear();

            A.CallTo(() => unitOfWork.Tasks.AddAsync(A<TaskEntity>.Ignored))
                .Invokes((TaskEntity task) => SeedData.Tasks.Add(task));

            A.CallTo(() => unitOfWork.Tasks.GetByIdAsync(A<int>.Ignored))
                .ReturnsLazily(fake =>
                System.Threading.Tasks.Task.Run(() => SeedData.Tasks.SingleOrDefault(n => n.Id == id)));

            A.CallTo(() => unitOfWork.Tasks.DeleteAt(id))
                .Invokes((int id) => SeedData.Tasks.RemoveAll(n => n.Id == id));

            //Act

            await handler.AddTaskAsync(new Modelling.DTOs.Task() { Id = id, State = initial });

            handler.DeleteTaskById(id);

            handler.AddTaskAsync(new Modelling.DTOs.Task() { Id = id, State = final });

            //Assert

            await Assert.ThrowsAsync<ArgumentException>
                 (async () => await handler.TryAddTaskAsync(new Modelling.DTOs.Task() { Id = id }));

            Assert.Single(SeedData.Tasks);

            Assert.Equal(SeedData.Tasks.Single(n => n.Id == id).State, final);
        }

        [Fact]
        public async System.Threading.Tasks.Task AddUserToTeam_WhenRightFK_ShouldPopulateNavigationProperty()
        {
            //Arrange

            SeedData.Users.Clear();
            SeedData.Teams.Clear();

            A.CallTo(() => unitOfWork.Users.AddAsync(A<UserEntity>.Ignored))
               .Invokes((UserEntity user) => SeedData.Users.Add(user));

            A.CallTo(() => unitOfWork.Teams.AddAsync(A<TeamEntity>.Ignored))
              .Invokes((TeamEntity team) => SeedData.Teams.Add(team));

            A.CallTo(() => unitOfWork.CompleteAsync())
                .Invokes(fake =>
               {
                   if (SeedData.Teams.Single().Id != SeedData.Users.Single().TeamEntityId)
                       throw new DbUpdateException();
               });

            //Act
            var createdTeam =
            handler.AddTeamAsync(new Team() { Name = "TeamName", Id = 1 });

            handler.AddUserAsync(new User() { Name = "UserName1", TeamId = 1 });

            //Asser

            Assert.Equal(SeedData.Teams.Single().Id, SeedData.Users.Single().TeamEntityId);
        }

        [Fact]
        public async System.Threading.Tasks.Task AddUserToTeam_WhenNotRightFK_ThrowsDbUpdateException()
        {
            //Arrange

            SeedData.Teams.Clear();
            SeedData.Users.Clear();

            A.CallTo(() => unitOfWork.Users.AddAsync(A<UserEntity>.Ignored))
               .Invokes((UserEntity user) => SeedData.Users.Add(user));

            A.CallTo(() => unitOfWork.Teams.AddAsync(A<TeamEntity>.Ignored))
              .Invokes((TeamEntity team) => SeedData.Teams.Add(team));

            A.CallTo(() => unitOfWork.CompleteAsync())
                .Invokes(fake =>
                {
                    if (SeedData.Teams.Single().Id != SeedData.Users.Single().TeamEntityId)
                        throw new DbUpdateException();
                });

            //Act
            var createdTeam =
            handler.AddTeamAsync(new Team() { Name = "TeamName", Id = 1 });

            //Assert

            Assert.ThrowsAsync<DbUpdateException>(async () => handler.AddUserAsync(new User() { Name = "UserName1", TeamId = 1 }));
        }
    }
}
