using System;
using Xunit;
using FakeItEasy;
using Projects.API.Interfaces;
using Projects.API.ExtensionsConfiguration;
using System.Threading.Tasks;
using System.Collections.Generic;
using Projects.Modelling.Entities;
using System.Linq;
using Projects.Modelling.DTOs;

namespace Projects.API.Services.Testing
{
    public class QueryProcessingServiceTests
    {
        private readonly IEntityHandlerService handler;
        private readonly IQueryProcessingService queryHandler;

        public QueryProcessingServiceTests()
        {
            SeedData.Init();

            handler = A.Fake<IEntityHandlerService>();

            queryHandler = new QueryProcessingService(handler);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetTasksQuantityPerProjectAsync_WhenUser1_GetTwoProjectsWith2And0RightTasks()
        {
            //Arrange

            A.CallTo(() => handler.GetAllProjectEntitiesAsync()).Returns(SeedData.Projects);

            //Act

            var tQuantityPerProject = new Dictionary<ProjectEntity, int>(await
                 queryHandler.GetTasksQuantityPerProjectAsync(1));

            //Assert

            Assert.Collection(
                tQuantityPerProject,
             n =>
             {
                 Assert.Equal(n.Value, 2);
                 Assert.StrictEqual<ProjectEntity>(n.Key,
                                                  SeedData.Projects
                                                  .Where(p => p.Id == n.Key.Id).FirstOrDefault());
             },
             n1 =>
             {
                 Assert.Equal(n1.Value, 0);
                 Assert.StrictEqual<ProjectEntity>(n1.Key,
                                                  SeedData.Projects
                                                  .Where(p => p.Id == n1.Key.Id).FirstOrDefault());
             });
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUnhandledTaskForUser_Whenuser2_GetThreeRightTasks()
        {
            //Arrange

            A.CallTo(() => handler.GetUserEntitybyIdAsync(A<int>.Ignored)).Returns(SeedData.Users.Single(u => u.Id == 2));

            //Act

            var unhandledTasks = await queryHandler.GetUnhandledTasksForUser(2);

            //Assert

            Assert.Collection(
                unhandledTasks,
                n =>
                {
                    Assert.Equal(n.Id, 3);
                    Assert.StrictEqual(SeedData.Tasks.Single(n => n.Id == 3), n);
                },
                n =>
                {
                    Assert.Equal(n.Id, 4);
                    Assert.StrictEqual(SeedData.Tasks.Single(n => n.Id == 4), n);
                });
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUnhandledTaskForUser_WhenUserWithNoTasks_GetEmptyCollection()
        {
            //Arrange

            A.CallTo(() => handler.GetUserEntitybyIdAsync(A<int>.Ignored)).Returns(new UserEntity() { Name = "NoTasksEntity", Tasks = null });

            //Act

            var unhandledTasks = await queryHandler.GetUnhandledTasksForUser(int.MaxValue);

            //Assert

            Assert.Empty(unhandledTasks);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetTasksPerPerformerAsync_WhenUser2_GetOneRightTask()
        {
            //Arrange

            A.CallTo(() => handler.GetAllTaskEntitiesAsync()).Returns(SeedData.Tasks);

            //Act

            var tasksForUser2 = await queryHandler.GetTasksPerPerformerAsync(2);

            //Assert

            Assert.Collection<TaskEntity>(
                tasksForUser2,
                n =>
                {
                    Assert.StrictEqual(n, SeedData.Tasks.Where(p => p.Id == n.Id).FirstOrDefault());
                });
        }

        [Fact]
        public async System.Threading.Tasks.Task GetTasksPerPerformerAsync_WhenExtremeCaseUser_GetEmptyCollection()
        {
            //Arrange

            A.CallTo(() => handler.GetAllTaskEntitiesAsync()).Returns(SeedData.Tasks);

            //Act

            var tasksForUserN = await queryHandler.GetTasksPerPerformerAsync(int.MaxValue);

            //Assert

            Assert.Empty(tasksForUserN);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetTasksPerPerformerFinishedThisYearAsync_WhenUser2_GetOneRightTaskInfo()
        {
            //Arrange

            A.CallTo(() => handler.GetAllTaskEntitiesAsync()).Returns(SeedData.Tasks);

            //Act

            var tasksForUser2 = await queryHandler.GetTasksPerPerformerFinishedThisYearAsync(2);

            //Assert

            Assert.Collection<TaskInfo>(
            tasksForUser2,
            n =>
            {
                Assert.Equal(n.Id, 2);
                Assert.Equal(n.Name, "This Task has more than fourty-five symbols in it's name");
            });


        }

        [Fact]
        public async System.Threading.Tasks.Task GetTasksPerPerformerFinishedThisYearAsync_WhenExtremeCaseUser_GetEmptyCollection()
        {
            //Arrange

            A.CallTo(() => handler.GetAllTaskEntitiesAsync()).Returns(SeedData.Tasks);

            //Act

            var tasksForUserN = await queryHandler.GetTasksPerPerformerFinishedThisYearAsync(int.MinValue);

            //Assert

            Assert.Empty(tasksForUserN);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetOldestTeamsAsync_WhenSeedData_GetOneRecordWithTwoRightUsers()
        {
            //Arrange
            A.CallTo(() => handler.GetAllTeamEntitiesAsync()).Returns(SeedData.Teams);

            //Act
            var oldestUsersInfo = await queryHandler.GetOldestTeamsAsync();

            //Assert
            Assert.Collection(
            oldestUsersInfo,
            n =>
            {
                Assert.Equal(n.Id, 1);
                Assert.Equal(n.Name, "Team1");
                Assert.Collection(n.Users,
                    u => Assert.Equal(u.Id, 2),
                    u1 => Assert.Equal(u1.Id, 1));
            });

        }

        [Fact]
        public async System.Threading.Tasks.Task GetTasksPerPerformerAlphabeticallyAsync_WhenSeedData_GetInOrder2413()
        {
            //Arrange
            
            A.CallTo(() => handler.GetAllTaskEntitiesAsync()).Returns(SeedData.Tasks);

            //Act

            var tasksPerPerformer = new Dictionary<UserEntity, List<TaskEntity>>(await
                 queryHandler.GetTasksPerPerformerAlphabeticallyAsync());

            //Assert

            Assert.Collection(
                tasksPerPerformer,
                n =>
                {
                    Assert.Equal(n.Key.Id, 2);
                    Assert.Collection(
                        n.Value,
                        t => Assert.Equal(t.Id, 4),
                        t1 => Assert.Equal(t1.Id, 2),
                        t2 => Assert.Equal(t2.Id, 3));
                },
                n1 =>
                {
                    Assert.Equal(n1.Key.Id, 1);
                });
        }

        [Theory]
        [InlineData(1,1,2,1,1)]
        [InlineData(3,2,2,0,null)]
        [InlineData(2, null, null, 2, 3)]

        public async System.Threading.Tasks.Task GetUserInfoAsync_WhenGivenId_GetExpectedPropertyValues(int Id,
                                                                                                        int? lastProjId,
                                                                                                        int? lastProjTasksQuantity,
                                                                                                        int? unhandledtasksQuantity,
                                                                                                        int? longestTaskid)
        {
            //Arrange

           A.CallTo(() => handler.GetUserEntitybyIdAsync(A<int>.That.IsEqualTo<int>(Id)))
             .Returns(SeedData.Users.Single(n => n.Id == Id));

            //Act

            var userInfo = await queryHandler.GetUserInfoAsync(Id);

            //Assert
            Assert.StrictEqual(userInfo.User, SeedData.Users.Single(n => n.Id == Id));
            Assert.Equal(userInfo?.LastProject?.Id, lastProjId);
            Assert.Equal(userInfo?.LastProjectTasksQuantity, lastProjTasksQuantity);
            Assert.Equal(userInfo?.UnhandledTasksQuantity, unhandledtasksQuantity);
            Assert.Equal(userInfo?.LongetsTask?.Id, longestTaskid);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetProjectsInfoAsync_()
        {
         //Arrange

            A.CallTo(() => handler.GetAllProjectEntitiesAsync()).Returns(SeedData.Projects);

            //Act

            var projectsInfo = await queryHandler.GetProjectsInfoAsync();

            //Assert

            Assert.Collection(
                projectsInfo,
                n =>
                {
                    Assert.StrictEqual(n.LongestTask, SeedData.Tasks.Single(n => n.Id == 2));
                    Assert.StrictEqual(n.ShortestTask, SeedData.Tasks.Single(n => n.Id == 2));
                    Assert.Equal(n.UsersQuantity, 2);
                },
                n1 =>
                {
                    Assert.StrictEqual(n1.LongestTask, SeedData.Tasks.Single(n => n.Id == 3));
                    Assert.StrictEqual(n1.ShortestTask, SeedData.Tasks.Single(n => n.Id == 3));
                    Assert.Equal(n1.UsersQuantity, 2);
                },
                n2 =>
                {
                    Assert.Null(n2.LongestTask);
                    Assert.Null(n2.LongestTask);
                    Assert.Equal(n2.UsersQuantity, 2);
                });
        }
    }
}
