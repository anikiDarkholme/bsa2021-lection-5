﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Projects.API.Interfaces;
using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.API.Controllers
{
    [ApiController]
    [Route("api/misc")]
    public class MiscellaneousController : ControllerBase
    {
        private readonly IQueryProcessingService queryHandler;
        private readonly IMapper mapper;

        public MiscellaneousController(IQueryProcessingService queryHandler, IMapper mapper)
        {
            this.queryHandler = queryHandler;
            this.mapper = mapper;
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Modelling.DTOs.Task>))]
        [Route("unhandledTasks/{id}")]
        public async Task<IActionResult> GetUnhandledTasksForUser(int id)
        {
            var unhandledTasks = await queryHandler.GetUnhandledTasksForUser(id);

            var unhandledDTOs = mapper.Map<IEnumerable<TaskEntity>, IEnumerable<Modelling.DTOs.Task>>(unhandledTasks);

            return Ok(unhandledDTOs);
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<KeyValuePair<ProjectEntity, int>>))]
        [Route("tasksQuantity/{id}")]
        public async Task<IActionResult> GetTasksQuantityPerProject(int id)
        {
            var quantityByProject = await queryHandler.GetTasksQuantityPerProjectAsync(id);

            return Ok(quantityByProject);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IList<TaskEntity>))]
        [Route("tasks/{id}")]
        public async Task<IActionResult> GetTasksPerPerformer(int id)
        {
            var TasksPerPerfomer = await queryHandler.GetTasksPerPerformerAsync(id);

            return Ok(TasksPerPerfomer);
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IList<TaskInfo>))]
        [Route("tasksThisYear/{id}")]
        public async Task<IActionResult> GetTasksPerPerformerThisYear(int id)
        {
            var TasksThisYear = await queryHandler.GetTasksPerPerformerFinishedThisYearAsync(id);

            return Ok(TasksThisYear);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<OldestUsersInfo>))]
        [Route("oldestTeams")]
        public async Task<IActionResult> GetOldestTeams()
        {
            var OldestTeams = await queryHandler.GetOldestTeamsAsync();

            return Ok(OldestTeams);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<KeyValuePair<UserEntity, List<TaskEntity>>>))]
        [Route("tasksAlpha")]
        public async Task<IActionResult> GetTasksPerPerformerAlphabetically()
        {
            var TasksPerPerfomer = await queryHandler.GetTasksPerPerformerAlphabeticallyAsync();

            return Ok(TasksPerPerfomer);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserInfo))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Route("userInfo/{id}")]
        public async Task<IActionResult> GetUserInfo(int id)
        {
            UserInfo userInfo;

            try
            {
             userInfo = await queryHandler.GetUserInfoAsync(id);
            }
            catch
            {
                return NotFound("No projects with this author");
            }

            return Ok(userInfo);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<ProjectInfo>))]
        [Route("projectsInfo")]
        public async Task<IActionResult> GetProjectsInfo()
        {
            var projectInfo = await queryHandler.GetProjectsInfoAsync();

            return Ok(projectInfo);
        }
    }
}
