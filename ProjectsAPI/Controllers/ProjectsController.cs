﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Projects.API.ExtensionsConfiguration;
using Projects.API.Interfaces;
using Projects.API.Services;
using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.API.Controllers
{
    [ApiController]
    [Route("api/projects")]
    public class ProjectsController : ControllerBase
    {
        private readonly IEntityHandlerService entityHandler;
        private readonly IDTOHandlerService dtoHandler;
        private readonly IMapper mapper;

        public ProjectsController(IEntityHandlerService entityHandler, IDTOHandlerService dtoHandler, IMapper mapper)
        {
            this.entityHandler = entityHandler;
            this.dtoHandler = dtoHandler;
            this.mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Project>))]
        public async Task<IActionResult> Get()
        {
           var projects = await entityHandler.GetAllProjectEntitiesAsync();

            var projectDTOs = mapper.Map<IEnumerable<ProjectEntity>, IEnumerable<Project>>(projects);

            return Ok(projectDTOs);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Project))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetByID(int id)
        {
            var project = await entityHandler.GetProjectEntitybyIdAsync(id);

            if (project == null)
                return NotFound();

            var projectDTO = mapper.Map<ProjectEntity, Project>(project);

            return Ok(projectDTO);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(ProjectEntity))]
        public async Task<IActionResult> Post([FromBody] Project project)
        {
            string errorMessage = string.Empty;

            try
            {
                var createdTask =
                await (dtoHandler as DTOHandlerService).TryAddProjectAsync(project);

                return Created("", createdTask);
            }
            catch (Exception ex)
            {
                errorMessage += ex.GetaAllMessages();
            }

            return BadRequest(errorMessage);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<IActionResult> Put(int id, [FromBody] Project project)
        {
            if (id < 0)
                return BadRequest("Invalid ID value. Must be greater or equal to 0");

            if (dtoHandler.DeleteProjectById(id))
            {
                return Ok(
                      await (dtoHandler as DTOHandlerService)
                      .AddProjectAsync(project));
            }
            else
            {
                return Created("",
                      await (dtoHandler as DTOHandlerService)
                        .AddProjectAsync(project));
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Delete(int id)
        {
            if (id < 1)
                return BadRequest("Invalid Id parameter. Must be greater than 0");

            if (dtoHandler.DeleteProjectById(id))
                return NoContent();

            else
                return Ok("No content found with the given id");
        }
    }
}
