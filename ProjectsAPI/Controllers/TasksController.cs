﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.API.Interfaces;
using Projects.API.Services;
using Projects.Modelling.Entities;
using System;
using Projects.API.ExtensionsConfiguration;
using AutoMapper;

namespace Projects.API.Controllers
{
    [ApiController]
    [Route("api/tasks")]
    public class TasksController : ControllerBase
    {
        private readonly IEntityHandlerService entityHandler;
        private readonly IDTOHandlerService dtoHandler;
        private readonly IMapper mapper;

        public TasksController(IEntityHandlerService entityHandler, IDTOHandlerService dtoHandler, IMapper mapper)
        {
            this.entityHandler = entityHandler;
            this.dtoHandler = dtoHandler;
            this.mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Task>))]
        public async Task<IActionResult> Get()
        {
            var Tasks = await entityHandler.GetAllTaskEntitiesAsync();

            var taskDTOs = mapper.Map<IEnumerable<TaskEntity>, IEnumerable<Modelling.DTOs.Task>>(Tasks);

            return Ok(taskDTOs);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Modelling.DTOs.Task))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetByID(int id)
        {
            var Task = await entityHandler.GetTaskEntitybyIdAsync(id);

            if (Task == null)
                return NotFound();

            var taskDTO = mapper.Map<TaskEntity, Modelling.DTOs.Task>(Task);

            return Ok(taskDTO);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(Modelling.DTOs.Task))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] Projects.Modelling.DTOs.Task Task)
        {
            string errorMessage = string.Empty;

            try
            {
                var createdTask = 
                await (dtoHandler as DTOHandlerService).TryAddTaskAsync(Task);

                    return Created("", createdTask);
            }
            catch(Exception ex)
            {
                errorMessage += ex.GetaAllMessages();
            }

            return BadRequest(errorMessage);   
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Put(int id, [FromBody] Modelling.DTOs.Task Task)
        {
            if (id < 0)
                return BadRequest("Invalid ID value. Must be greater or equal to 0");

            if (dtoHandler.DeleteTaskById(id))
            {
                return Ok(
                      await (dtoHandler as DTOHandlerService)
                      .AddTaskAsync(Task));
            }
            else
            {
                return Created("",
                      await (dtoHandler as DTOHandlerService)
                        .AddTaskAsync(Task));
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Delete(int id)
        {
            if (id < 1)
                return BadRequest("Invalid Id parameter. Must be greater than 0");

            if (dtoHandler.DeleteTaskById(id))
                return NoContent();

            else
                return Ok("No content found with the given id");
        }
    }
}
