﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.API.Services;
using Projects.Modelling.Entities;
using Projects.API.Interfaces;
using Projects.Modelling.DTOs;
using System;
using Projects.API.ExtensionsConfiguration;
using AutoMapper;

namespace Projects.API.Controllers
{
    [ApiController]
    [Route("api/teams")]
    public class TeamsController : ControllerBase
    {
        private readonly IEntityHandlerService entityHandler;
        private readonly IDTOHandlerService dtoHandler;
        private readonly IMapper mapper;

        public TeamsController(IEntityHandlerService entityHandler, IDTOHandlerService dtoHandler, IMapper mapper)
        {
            this.entityHandler = entityHandler;
            this.dtoHandler = dtoHandler;
            this.mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Team>))]
        public async Task<IActionResult> Get()
        {
            var Teams = await entityHandler.GetAllTeamEntitiesAsync();

            var userDTOs = mapper.Map<IEnumerable<TeamEntity>, IEnumerable<Team>>(Teams);

            return Ok(Teams);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Team))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetByID(int id)
        {
            var Team = await entityHandler.GetTeamEntitybyIdAsync(id);

            if (Team == null)
                return NotFound();

            var teamDTO = mapper.Map<TeamEntity, Team>(Team);

            return Ok(teamDTO);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(TeamEntity))]
        public async Task<IActionResult> Post([FromBody] Team Team)
        {
            string errorMessage = string.Empty;

            try
            {
                var createdTeam =
                await (dtoHandler as DTOHandlerService).TryAddTeamAsync(Team);

                return Created("", createdTeam);
            }
            catch (Exception ex)
            {
                errorMessage += ex.GetaAllMessages();
            }

            return BadRequest(errorMessage);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Put(int id, [FromBody] Team Team)
        {
            if (id < 0)
                return BadRequest("Invalid ID value. Must be greater or equal to 0");

            if (dtoHandler.DeleteTeamById(id))
            {
                return Ok(
                      await (dtoHandler as DTOHandlerService)
                      .AddTeamAsync(Team));
            }
            else
            {
                return Created("",
                      await (dtoHandler as DTOHandlerService)
                        .AddTeamAsync(Team));
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Delete(int id)
        {
            if (id < 1)
                return BadRequest("Invalid Id parameter. Must be greater than 0");

            if (dtoHandler.DeleteTeamById(id))
                return NoContent();

            else
                return Ok("No content found with the given id");
        }
    }
}
