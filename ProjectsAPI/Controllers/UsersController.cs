﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.API.Services;
using Projects.Modelling.Entities;
using Projects.API.Interfaces;
using Projects.Modelling.DTOs;
using System;
using Projects.API.ExtensionsConfiguration;
using AutoMapper;

namespace Projects.API.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private readonly IEntityHandlerService entityHandler;
        private readonly IDTOHandlerService dtoHandler;
        private readonly IMapper mapper;

        public UsersController(IEntityHandlerService entityHandler,
            IDTOHandlerService dtoHandler,
            IMapper mapper)
        {
            this.entityHandler = entityHandler;
            this.dtoHandler = dtoHandler;
            this.mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<User>))]
        public async Task<IActionResult> Get()
        {
            var Users = await entityHandler.GetAllUserEntitiesAsync();

            var userDTOs = mapper.Map<IEnumerable<UserEntity>, IEnumerable<User>>(Users);

            return Ok(userDTOs);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(User))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> GetByID(int id)
        {
            var User = await entityHandler.GetUserEntitybyIdAsync(id);

            if (User == null)
                return NoContent();

            var userDTO = mapper.Map<UserEntity, User>(User);

            return Ok(userDTO);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(UserEntity))]
        public async Task<IActionResult> Post([FromBody] User User)
        {
            string errorMessage = string.Empty;

            try
            {
                var createdUser =
                await (dtoHandler as DTOHandlerService).TryAddUserAsync(User);

                return Created("", createdUser);
            }
            catch (Exception ex)
            {
                errorMessage += ex.GetaAllMessages();
            }

            return BadRequest(errorMessage);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Put(int id, [FromBody] User User)
        {
            if (id < 0)
                return BadRequest("Invalid ID value. Must be greater or equal to 0");

            if (dtoHandler.DeleteUserById(id))
            {
                return Ok(
                      await (dtoHandler as DTOHandlerService)
                      .AddUserAsync(User));
            }
            else
            {
                return Created("",
                      await (dtoHandler as DTOHandlerService)
                        .AddUserAsync(User));
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Delete(int id)
        {
            if (id < 1)
                return BadRequest("Invalid Id parameter. Must be greater than 0");

            if (dtoHandler.DeleteUserById(id))
                return NoContent();

            else
                return Ok("No content found with the given id");
        }
    }
}
