﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Projects.API.ExtensionsConfiguration
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            SeedData.Init();

            modelBuilder.Entity<ProjectEntity>()
                .HasData(SeedData.Projects);

            modelBuilder.Entity<TaskEntity>()
                .HasData(SeedData.Tasks);

            modelBuilder.Entity<User>()
                .HasData(SeedData.Users);

            modelBuilder.Entity<Team>()
                .HasData(SeedData.Teams);
        }
    }
}
