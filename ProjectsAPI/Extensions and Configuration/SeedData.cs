﻿using Bogus;
using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.API.ExtensionsConfiguration
{
	// Tests hard depend on Seed data. Changing any id's, fake functions or else may and WILL break tests!
	public static class SeedData
	{
		private static Faker f;

		public static List<ProjectEntity> Projects = new List<ProjectEntity>();

		public static List<TaskEntity> Tasks = new List<TaskEntity>();
			
		public static List<TeamEntity> Teams = new List<TeamEntity>();

		public static List<UserEntity> Users = new List<UserEntity>();

		public static User UUser { get; set; }

		public static Project UProject { get; set; }

		public static Team UTeam { get; set; }

		public static Modelling.DTOs.Task UTask { get; set; }

		public static void Init()
		{
			f = new Faker();

			Projects = new List<ProjectEntity>();

			Tasks = new List<TaskEntity>();

			Teams = new List<TeamEntity>();

			Users = new List<UserEntity>();

			var team1 = new TeamEntity()
			{
				Id = 1,
				CreatedAt = f.Date.Past(),
				Name = "Team1",
				Users = new List<UserEntity>(),
	            Projects = new List<ProjectEntity>(),
			};
			var team2 = new TeamEntity()
			{
				Id = 2,
				CreatedAt = f.Date.Past(),
				Name = "Team2",
				Users = new List<UserEntity>(),
				Projects = new List<ProjectEntity>(),
			};

			var user1 = new UserEntity()
			{
				BirthDay = new DateTime(1960,10,1),
				Email = f.Internet.Email(),
				FirstName = "Brian",
				Id = 1,
				LastName = f.Name.LastName(),
				RegisteredAt = new DateTime(1987,11,24),
				Tasks = new List<TaskEntity>(),
				Projects = new List<ProjectEntity>(),
				TeamEntity = team1,
				TeamEntityId = 1,
			};
			var user2 = new UserEntity()
			{
				BirthDay = new DateTime(1960, 11, 1),
				Email = f.Internet.Email(),
				FirstName = "Bach",
				Id = 2,
				LastName = f.Name.LastName(),
				RegisteredAt = new DateTime(1987, 11, 25),
				Tasks = new List<TaskEntity>(),
				Projects = new List<ProjectEntity>(),
				TeamEntityId = 1,
				TeamEntity = team1,
			};
			var user3 = new UserEntity()
			{
				BirthDay = f.Date.Recent(),
				Email = f.Internet.Email(),
				FirstName = "Zoey",
				Id = 3,
				LastName = f.Name.LastName(),
				RegisteredAt = f.Date.Past(),
				Tasks = new List<TaskEntity>(),
				Projects = new List<ProjectEntity>(),
				TeamEntity = team2,
				TeamEntityId = 2
			};
			var user4 = new UserEntity()
			{
				BirthDay = f.Date.Recent(),
				Email = f.Internet.Email(),
				FirstName = "Barney",
				Id = 4,
				LastName = f.Name.LastName(),
				RegisteredAt = f.Date.Past(),
				Tasks = new List<TaskEntity>(),
				Projects = new List<ProjectEntity>(),
				TeamEntity = team2,
				TeamEntityId = 2
			};


			(team1.Users as List<UserEntity>).AddRange(new List<UserEntity>{user1, user2});
			(team2.Users as List<UserEntity>).AddRange(new List<UserEntity> { user3, user4 });


			var task1 = new TaskEntity()
			{
				Id = 1,
				CreatedAt = f.Date.Past(),
				Description = "This task has a short description",
				Name = "This Task has the largest name. This string has much more characters than all the others.",
				State = Modelling.DTOs.State.InProgress,
				FinishedAt = new DateTime(2015,02,02),
				UserEntity = user1,
				UserEntityId = 1,				
			};
			var task2 = new TaskEntity()
			{
				Id = 2,
				CreatedAt = new DateTime(2014,01,11),
				Description = "This Task has more than fourty-five symbols in it's name and finished within last two days",
				Name = "This Task has more than fourty-five symbols in it's name",
				State = Modelling.DTOs.State.Finished,
				FinishedAt = DateTime.Now,
				UserEntity = user2,
				UserEntityId = 2,

			};
			var task3 = new TaskEntity()
			{
				Id = 3,
				CreatedAt = new DateTime(2014,01,11),
				Description = "This Task has >45 symbols in it's name and finished at more than 5 years ago",
				Name = "Less than 45 symbols in this name",
				State = Modelling.DTOs.State.InProgress,
				FinishedAt = null,
				UserEntity= user2,
				UserEntityId = 2,
			};
			var task4 = new TaskEntity()
			{
				Id = 4,
				CreatedAt = new DateTime(2014,01,12),
				Description = "Less than 45 symbols in this name still and is not yet finished",
				Name = "This Task has more than fourty-five symbols in it's name.",
				State = Modelling.DTOs.State.InProgress,
				FinishedAt = null,
				UserEntity = user2,
				UserEntityId = 2,
			};


			(user1.Tasks as List<TaskEntity>).AddRange(new List<TaskEntity> { task1 });
			(user2.Tasks as List<TaskEntity>).AddRange(new List<TaskEntity> { task2, task3, task4});

			var project1 = new ProjectEntity()
			{
				CreatedAt = new DateTime(1990,11,11),
				Deadline = f.Date.Future(),
				Description = "Project 1 Description",
				Id = 1,
				Name = f.Company.CompanyName(),
				UserEntity = user1,
				UserEntityId = 1,
				Tasks = new List<TaskEntity>(),
				TeamEntity = team1,
				TeamEntityId = 1
			};

			var project2 = new ProjectEntity()
			{
				CreatedAt = new DateTime(1990,12,11),
				Deadline = f.Date.Future(),
				Description = "Project 2 Description",
				Id = 2,
				Name = f.Company.CompanyName(),
				UserEntity = user3,
				UserEntityId = 3,
				Tasks = new List<TaskEntity>(),
				TeamEntity = team2,
				TeamEntityId = 2
			};

			var project3 = new ProjectEntity()
			{
				CreatedAt = new DateTime(1990, 10, 11),
				Deadline = f.Date.Future(),
				Description = "Project 3 Description",
				Id = 3,
				Name = f.Company.CompanyName(),
				UserEntity = user1,
				UserEntityId = 1,
				Tasks = new List<TaskEntity>(),
				TeamEntity = team1,
				TeamEntityId = 1
			};

			(project1.Tasks as List<TaskEntity>).AddRange(new List<TaskEntity> { task1, task2 });
			(project2.Tasks as List<TaskEntity>).AddRange(new List<TaskEntity> { task3, task4 });

			task1.ProjectEntity = project1;
			task1.ProjectEntityId = 1;

			task2.ProjectEntity = project1;
			task2.ProjectEntityId = 1;

			task3.ProjectEntity = project2;
			task3.ProjectEntityId = 2;

			(team1.Projects as List<ProjectEntity>).AddRange(new List<ProjectEntity> { project1 });
			(user1.Projects as List<ProjectEntity>).AddRange(new List<ProjectEntity> { project1, project3 });
			(user3.Projects as List<ProjectEntity>).AddRange(new List<ProjectEntity> { project2 });

			Teams.AddRange(new List<TeamEntity> { team1, team2 });
			Projects.AddRange(new List<ProjectEntity> { project1, project2, project3 });
			Users.AddRange(new List<UserEntity> { user1, user2, user3, user4 });
			Tasks.AddRange(new List<TaskEntity> { task1, task2, task3, task4 });

			var Uproject1 = new Project()
			{
				CreatedAt = new DateTime(1990, 10, 11),
				Deadline = f.Date.Future(),
				Description = f.Lorem.Sentence(),
				Name = f.Company.CompanyName()
			};

			UProject = Uproject1;

			var Uuser1 = new User()
			{
				BirthDay = new DateTime(1960, 10, 1),
				Email = f.Internet.Email(),
				FirstName = "Brian",
				LastName = f.Name.LastName(),
				RegisteredAt = new DateTime(1987, 11, 24),
			};

			UUser = Uuser1;

			var Uteam1 = new Team()
			{ 
				CreatedAt = f.Date.Past(),
				Name = "Team1",
			};

			UTeam = Uteam1;

			var Utask1 = new Modelling.DTOs.Task()
			{
				CreatedAt = new DateTime(2014, 01, 11),
				Description = "This Task has >45 symbols in it's name and finished at more than 5 years ago",
				Name = "Less than 45 symbols in this name",
				State = Modelling.DTOs.State.InProgress,
				FinishedAt = null,
			};

			UTask = Utask1;
		}
	}
}

