﻿using Projects.Modelling.DTOs;
using System.Threading.Tasks;

namespace Projects.API.Interfaces
{
    public interface IDTOHandlerService
    {
        Task<Project> AddProjectAsync(Project project);
        Task<Modelling.DTOs.Task> AddTaskAsync(Modelling.DTOs.Task task);
        Task<Team> AddTeamAsync(Team team);
        Task<User> AddUserAsync(User user);
        Task<Project> TryAddProjectAsync(Project project);
        Task<Modelling.DTOs.Task> TryAddTaskAsync(Modelling.DTOs.Task task);
        Task<Team> TryAddTeamAsync(Team team);
        Task<User> TryAddUserAsync(User user);
        bool DeleteProjectById(int id);
        bool DeleteTaskById(int id);
        bool DeleteTeamById(int id);
        bool DeleteUserById(int id);
    }
}