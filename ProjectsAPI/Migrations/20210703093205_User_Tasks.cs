﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Projects.API.Migrations
{
    public partial class User_Tasks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Teams_TeamEntityId",
                table: "Users");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Teams_TeamEntityId",
                table: "Users",
                column: "TeamEntityId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Teams_TeamEntityId",
                table: "Users");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Teams_TeamEntityId",
                table: "Users",
                column: "TeamEntityId",
                principalTable: "Teams",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
