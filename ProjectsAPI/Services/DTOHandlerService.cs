﻿using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Projects.API.Interfaces;
using Projects.DataAccess.Interfaces;
using Projects.DataAccess.Repositories;
using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System;
using System.Threading.Tasks;

namespace Projects.API.Services
{
    public class DTOHandlerService : IDTOHandlerService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public DTOHandlerService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<Modelling.DTOs.Task> AddTaskAsync(Modelling.DTOs.Task task)
        {
            var mappedEntity =
                mapper.Map<Modelling.DTOs.Task, TaskEntity>(task);

            var returnedEntity =
            await unitOfWork.Tasks.AddAsync(mappedEntity);

            try
            {
            await unitOfWork.CompleteAsync();
            }
            catch (DbUpdateException)
            {
                throw;
            }

            var returnedDTO =
                mapper.Map<TaskEntity, Modelling.DTOs.Task>(returnedEntity);

            return returnedDTO;
        }

        public async Task<User> AddUserAsync(User user)
        {
            var mappedEntity =
                 mapper.Map<User, UserEntity>(user);

            var returnedEntity =
            await unitOfWork.Users.AddAsync(mappedEntity);

            try
            {
                await unitOfWork.CompleteAsync();
            }
            catch (DbUpdateException)
            {
                throw;
            }

            var returnedDTO =
                mapper.Map<UserEntity, User>(returnedEntity);

            return returnedDTO;
        }

        public async Task<Team> AddTeamAsync(Team team)
        {
            var mappedEntity =
                mapper.Map<Team, TeamEntity>(team);

            var returnedEntity =
            await unitOfWork.Teams.AddAsync(mappedEntity);

            try
            {
                await unitOfWork.CompleteAsync();
            }
            catch (DbUpdateException)
            {
                throw;
            }

            var returnedDTO =
                mapper.Map<TeamEntity, Team>(returnedEntity);

            return returnedDTO;
        }

        public async Task<Project> AddProjectAsync(Project project)
        {
            var mappedEntity =
                mapper.Map<Project, ProjectEntity>(project);

            var returnedEntity =
            await unitOfWork.Projects.AddAsync(mappedEntity);

            try
            {
                await unitOfWork.CompleteAsync();
            }
            catch (DbUpdateException)
            {
                throw;
            }

            var returnedDTO =
                mapper.Map<ProjectEntity, Project>(returnedEntity);

            return returnedDTO;
        }

        public async Task<Modelling.DTOs.Task> TryAddTaskAsync(Modelling.DTOs.Task task)
        {
            if (await unitOfWork.Tasks.GetByIdAsync(task.Id) != null)
                   throw new ArgumentException($"Task Id { task.Id } already exists in the database");

            return await AddTaskAsync(task);
        }

        public async Task<User> TryAddUserAsync(User user)
        {
            if (await unitOfWork.Users.GetByIdAsync(user.Id) != null)
                throw new ArgumentException($"Task Id { user.Id } already exists in the database");

            return await AddUserAsync(user);
        }

        public async Task<Team> TryAddTeamAsync(Team team)
        {
            if (await unitOfWork.Teams.GetByIdAsync(team.Id) != null)
                throw new ArgumentException($"Task Id { team.Id } already exists in the database");

            return await AddTeamAsync(team);
        }

        public async Task<Project> TryAddProjectAsync(Project project)
        {
            if (await unitOfWork.Projects.GetByIdAsync(project.Id) != null)
                throw new ArgumentException($"Task Id { project.Id } already exists in the database");

            return await AddProjectAsync(project);
        }

        public bool DeleteProjectById(int id)
        {
            if (unitOfWork.Projects.GetById(id) == null)
                return false;

            unitOfWork.Projects.DeleteAt(id);

            unitOfWork.Complete();

            return true;
        }

        public bool DeleteTaskById(int id)
        {
            if (unitOfWork.Tasks.GetById(id) == null)
                return false;

            unitOfWork.Tasks.DeleteAt(id);

            unitOfWork.Complete();

            return true;
        }

        public bool DeleteTeamById(int id)
        {
            if (unitOfWork.Teams.GetById(id) == null)
                return false;

            unitOfWork.Teams.DeleteAt(id);

            unitOfWork.Complete();

            return true;
        }

        public bool DeleteUserById(int id)
        {
            if (unitOfWork.Users.GetById(id) == null)
                return false;

            unitOfWork.Users.DeleteAt(id);

            unitOfWork.Complete();

            return true;
        }
    }
}

