﻿using Projects.DataAccess.Interfaces;
using Projects.DataAccess.Repositories;
using Projects.Modelling.Entities;
using Projects.Modelling.Interfaces;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Projects.API.Interfaces;
using Projects.Modelling.DTOs;

namespace Projects.API.Services
{
    public class EntityHandlerService : IEntityHandlerService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IEntityBinderService binder;

        public EntityHandlerService(IUnitOfWork unitOfWork, IEntityBinderService binder)
        {
            this.unitOfWork = unitOfWork;
            this.binder = binder;
        }

        public async Task<IEnumerable<ProjectEntity>> GetAllProjectEntitiesAsync()
        {
            var projectModels = await
                (unitOfWork.Projects as ProjectRepository)
                .QueryAsync(
                    n => true,
                    n => n.Tasks, n => n.TeamEntity, n => n.UserEntity);

            return projectModels;
        }

        public async Task<ProjectEntity> GetProjectEntitybyIdAsync(int id)
        {
            var projectEntity = await 
                (unitOfWork.Projects as ProjectRepository)
                .GetByIdAsync(id,
                n => n.Tasks, n => n.TeamEntity, n => n.UserEntity);

            return projectEntity;
        }

        public async Task<IEnumerable<TaskEntity>> GetAllTaskEntitiesAsync()
        {
            var tasksEntities = await
                (unitOfWork.Tasks as TaskRepository)
                .QueryAsync(
                    n => true,
                    n => n.UserEntity,
                    n => n.ProjectEntity);

            return tasksEntities;
        }

        public async Task<TaskEntity> GetTaskEntitybyIdAsync(int id)
        {
            var taskEntity = await
                (unitOfWork.Tasks as TaskRepository)
                .GetByIdAsync(id,
                n => n.UserEntity,
                n => n.ProjectEntity);

            return taskEntity;
        }

        public async Task<IEnumerable<UserEntity>> GetAllUserEntitiesAsync()
        {
            var userEntities = await
                (unitOfWork.Users as UserRepository)
                .QueryAsync(
                    n => true,
                    n => n.Projects,
                    n => n.Tasks,
                    n => n.TeamEntity);

            return userEntities;
        }

        public async Task<UserEntity> GetUserEntitybyIdAsync(int id)
        {
            var userEntity = await
                  (unitOfWork.Users as UserRepository)
                  .GetByIdAsync(id,
                  n => n.Projects,
                  n => n.Tasks,
                    n => n.TeamEntity);

            return userEntity;
        }

        public async Task<IEnumerable<TeamEntity>> GetAllTeamEntitiesAsync()
        {
            var teamEntities = await
                (unitOfWork.Teams as TeamRepository)
                .QueryAsync(
                    n => true,
                    n => n.Users,
                    n => n.Projects);

            return teamEntities;
        }

        public async Task<TeamEntity> GetTeamEntitybyIdAsync(int id)
        {
            var teamEntity = await
                (unitOfWork.Teams as TeamRepository)
                .GetByIdAsync(id,
                n => n.Users,
                n => n.Projects);

            return teamEntity;
        }
    }
}
