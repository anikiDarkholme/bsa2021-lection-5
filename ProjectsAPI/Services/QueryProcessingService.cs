﻿using Projects.API.Interfaces;
using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects.API.Services
{
    public class QueryProcessingService : IQueryProcessingService
    {
        private readonly IEntityHandlerService handler;

        public QueryProcessingService(IEntityHandlerService handler)
        {
            this.handler = handler;
        }

        public async Task<IEnumerable<KeyValuePair<ProjectEntity, int>>> GetTasksQuantityPerProjectAsync(int userId)
        {
            var projects = await handler.GetAllProjectEntitiesAsync();

            var tasksQuantityPerProject = new List<KeyValuePair<ProjectEntity, int>>();

            tasksQuantityPerProject.AddRange(
                                    projects
                                    .Where(n => n.UserEntity.Id == userId)
                                    .Select(n => new KeyValuePair<ProjectEntity, int>(n, n.Tasks.ToList().Count))
                                    ?? new List<KeyValuePair<ProjectEntity, int>>());

            return tasksQuantityPerProject;
        }

        public async Task<IList<TaskEntity>> GetTasksPerPerformerAsync(int performerId)
        {
            var tasks = await handler.GetAllTaskEntitiesAsync();

            var tasksperPerPerformer = new List<TaskEntity>();

            try
            {
                tasksperPerPerformer.AddRange(
                tasks
                  .Where(n => n.UserEntity.Id == performerId)
                  .Where(n => n.Name.Count() < 45)
                  ?? new List<TaskEntity>());

                return tasksperPerPerformer;
            }
            catch { throw; }
        }

        public async Task<IList<TaskInfo>> GetTasksPerPerformerFinishedThisYearAsync(int performerId)
        {
            var tasks = await handler.GetAllTaskEntitiesAsync();

            var tasksperPerformer = new List<TaskInfo>();

            tasksperPerformer.AddRange(
                tasks
                .Where(n => n.UserEntity.Id == performerId)
                .Where(n => n.State == State.Finished)
                .Where(n => n.FinishedAt?.Year == DateTime.Now.Year)
                .Select(n => new TaskInfo() { Id = n.Id, Name = n.Name })
                ?? new List<TaskInfo>());

            return tasksperPerformer;
        }

        public async Task<IEnumerable<OldestUsersInfo>> GetOldestTeamsAsync()
        {
            var teams = await handler.GetAllTeamEntitiesAsync();

            var oldestUsers = new List<OldestUsersInfo>();

            oldestUsers.AddRange(
                teams.Where(team =>
                          team.Users.All(user => (DateTime.Now.Year - user.BirthDay.Year) > 10))
                  .Select(n => new OldestUsersInfo()
                  {
                      Id = n.Id,
                      Name = n.Name,
                      Users = n.Users
                               .OrderByDescending(user => user.RegisteredAt)
                  })
                  ?? new List<OldestUsersInfo>());

            return oldestUsers;
        }

        public async Task<IEnumerable<KeyValuePair<UserEntity, List<TaskEntity>>>> GetTasksPerPerformerAlphabeticallyAsync()
        {
            var tasks = await handler.GetAllTaskEntitiesAsync();

            var tasksPerPerformer = new List<KeyValuePair<UserEntity, List<TaskEntity>>>();

            tasksPerPerformer.AddRange(
                tasks
                  .OrderByDescending(task => task.Name.Length)
                  .GroupBy(task => task.UserEntity)
                  .OrderBy(n => n.Key.FirstName)

                  .Select(n => new KeyValuePair<UserEntity, List<TaskEntity>>(n.Key, n.ToList()))
                  ?? new List<KeyValuePair<UserEntity, List<TaskEntity>>> ());

            return tasksPerPerformer;
        }

        public async Task<UserInfo> GetUserInfoAsync(int userId)
        {
            var user = await
                handler.GetUserEntitybyIdAsync(userId);

            try
            {
                return
                    new UserInfo()
                    {
                        User = user,
                        LastProject = user.Projects
                                          .DefaultIfEmpty()
                                          .Aggregate((a, b) => b.CreatedAt > a.CreatedAt ? b : a),

                        LastProjectTasksQuantity = user.Projects
                                                       .DefaultIfEmpty()?
                                                       .Aggregate((a, b) => b.CreatedAt > a.CreatedAt ? b : a)?
                                                       .Tasks?
                                                       .Count(),

                        UnhandledTasksQuantity = user.Tasks
                                                     .Where(n => n.State == State.Canceled || n.State == State.InProgress)
                                                     .Count(),

                        LongetsTask = user?.Tasks
                                          .DefaultIfEmpty()
                                          .Aggregate((a, b) =>
                                          {
                                              if (!b.FinishedAt.HasValue)
                                              {
                                                  if (a.FinishedAt.HasValue)
                                                      return b;

                                                  else
                                                      return b.CreatedAt < a.CreatedAt ? b : a;
                                              }
                                              else
                                              {
                                                  if (!a.FinishedAt.HasValue)
                                                      return b.CreatedAt < a.CreatedAt ? b : a;

                                                  else
                                                      return (b.FinishedAt - b.CreatedAt).Value > (a.FinishedAt - a.CreatedAt).Value ? b : a;
                                              }
                                          })
                    };
            }
            catch { throw; }
        }

        public async Task<IEnumerable<ProjectInfo>> GetProjectsInfoAsync()
        {
            var projects = await handler.GetAllProjectEntitiesAsync();

            var projectsInfo = new List<ProjectInfo>();

            projectsInfo.AddRange(
                projects.
                Select(prj => new ProjectInfo()
                {
                    Project = prj,
                    LongestTask = prj.Tasks.Count() > 0 ?
                                  prj.Tasks.DefaultIfEmpty().Aggregate((a, b) => b.Description.Length > a.Description.Length ? b : a) :
                                  null,
                    ShortestTask = prj.Tasks.Count() > 0 ?
                                   prj.Tasks.DefaultIfEmpty().Aggregate((a, b) => b.Name.Length < a.Name.Length ? b : a) :
                                   null,
                    UsersQuantity = (prj.Description.Length > 20 || prj.Tasks.Count() < 3) ?
                                                              prj.TeamEntity.Users.Count() :
                                                              0
                })
                ?? new List<ProjectInfo>());

            return projectsInfo;
        }

        public async Task<IEnumerable<TaskEntity>> GetUnhandledTasksForUser(int id)
        {
            var user = await handler.GetUserEntitybyIdAsync(id);

            var unhandledTasks = new List<TaskEntity>();
            
            unhandledTasks.AddRange( user?.Tasks?.Where(n => n.State != State.Finished)
                                     ?? new List<TaskEntity>());

            return unhandledTasks;
        }
    }
}
