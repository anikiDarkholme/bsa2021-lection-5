﻿using Projects.Modelling;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Projects.DataAccess.Interfaces
{
    public interface IRepository<T>
    {
        T GetById(int id);

        Task<T> GetByIdAsync(int id,
            params Expression<Func<T, object>>[] includeExpression);

        IEnumerable<T> GetAll();

        T Add(T entity);

        Task<T> AddAsync(T entity);

        void DeleteAt(int id);

        int Count();
    }
}
