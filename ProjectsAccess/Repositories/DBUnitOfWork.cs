﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Projects.DataAccess.Interfaces;
using Projects.Modelling;
using Projects.Modelling.Entities;
using System;
using System.Threading.Tasks;

namespace Projects.DataAccess.Repositories
{
    public class DBUnitOfWork : IUnitOfWork
    {

        public DBUnitOfWork(
                             DbContext context,
                             IProjectRepository projects,
                             ITaskRepository tasks,
                             ITeamRepository teams,
                             IUserRepository users)
        {
            this.context = context;
            Projects = projects;
            Tasks = tasks;
            Teams = teams;
            Users = users;
        }

        private readonly DbContext context;

        public IProjectRepository Projects { get; }

        public ITaskRepository Tasks { get; }

        public ITeamRepository Teams { get; }

        public IUserRepository Users { get; }

        public void Complete()
        {
            context.SaveChanges();
        }

        //Throws SqlException when query didn't finish succesfully
        public async Task CompleteAsync()
        {
            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateException ex) { throw; }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
