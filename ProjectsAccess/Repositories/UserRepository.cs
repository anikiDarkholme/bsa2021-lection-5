﻿using Microsoft.EntityFrameworkCore;
using Projects.DataAccess.Interfaces;
using Projects.Modelling.DTOs;
using Projects.Modelling.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.DataAccess.Repositories
{
    public class UserRepository : DBRepository<UserEntity>, IUserRepository
    {
        public UserRepository(DbContext context)
        : base(context)
        {
        }
    }
}
